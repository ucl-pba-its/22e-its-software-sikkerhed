---
title: '22E ITS Software sikkerhed eksamen'
subtitle: 'Eksamens rapport'
filename: '22e_software_sikkerhed_exam.md'
authors: ['Martin Edwin Schjødt Nielsen \<mesn@ucl.dk\>']
main_author: 'Martin Edwin Schjødt Nielsen'
date: 2022-30-08
email: 'mesn@ucl.dk'
left-header: \today
right-header: Eksamens rapport
skip-toc: false
semester: 22E
---

# Dokumentets indhold
Læringsmålene for prøven er identiske med læringsmålene for det nationale fagelement af samme navn og fremgår af den nationale studieordning.


# Eksamens beskrivelse
Prøven består af en skriftlig aflevering (individuelt eller i en gruppe med op til tre
studerende) fulgt af en individuel mundtlig eksamen.
Den skriftlige aflevering må maksimum være 10 normalsider foruden bilag. Ved to
studerende er det maksimale antal 15 normalsider. Ved tre studerende er det maksimale
antal 20 normalsider. En normalside er 2.400 tegn inkl. mellemrum og fodnoter.
Forside, indholdsfortegnelse, litteraturliste samt bilag tæller ikke med heri. Bilag er uden for
bedømmelse og kan ikke forventes læst. For nærmere detaljer se afsnittet om ”Krav til
skriftlige prøver og projekter”.
Den mundtlig prøve er individuel og varer 25 minutters inkl. votering.


# Bedømmelse
Prøven bedømmes efter 7-trinsskalaen med ekstern bedømmelse. Bedømmelsen sker
som en helhedsvurdering af den skriftlige og mundtlige præstation.

# Afleverings dato for rapport.(Afleveres på wiseflow)  
1. forsøg - 9. December 2022.  
  

# Mundtlig Eksamens datoer.  
 1. forsøg - 16. December 2022
 2. forsøg - 25. Jannuar 2022
 3. forsøg - 22- Februar 2022.

