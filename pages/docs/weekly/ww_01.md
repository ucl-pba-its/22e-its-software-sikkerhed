---
Week: 36
Content: Hvad er software sikkerhed
Material: Se ugentligt plan
Initials: MESN
---

# Uge 36 - Introduktion til faget og opsætning af udviklingsmiljø.

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål
- Hver studerende har opsat .Net 6 SDK på sin pc
- Hver studerende har installeret en IDE eller text editor til C#
- Hver studerende har installeret postman.
- Hver studerende har bygget en "Hello world" konsol applikation i C#.
- Hver studerende har bygget en "Hello world" http server applikation i C#. 
### Læringsmål
- den studerende har viden om "Security by design".
- den studerende kan bruge et api/standard biblotekter.

## Afleveringer


## Skema
### Torsdag
|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 8:45  | Vidensdelings øvelser K1/K4          |
| 09:30 | Opsamling på vidensdeling                |
| 09:45 | pause                  |
| 10:15 | C# øvelser k1/k4       |
| 11:30 | Aftrædelse til selvstændig studie og gruppe arbejde |

## Øvelser

Se denne uges øvelser: [https://ucl-pba-its.gitlab.io/exercises/software/software_opgave_Uge36/](https://ucl-pba-its.gitlab.io/exercises/software/software_opgave_Uge36/)

## Kommentarer

- ..
