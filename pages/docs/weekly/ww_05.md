---
Week: 40
Content: Domæne primitiver
Material: Se ugentligt plan
Initials: MESN
---

# Uge 40 - Modellering

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål
- Hver studerende har udarbejdet en klasse med domæne primitiver.
- Hver studerende har udført en taint analyse af python kildekode.
- Hver studerende har udført en taint analyse af C# kildekode.
- Hver studerende har implemneteret en domæne primitive med read once mønsteret.

### Læringsmål
- Den studerende har viden om privacy by design principper (Read once objects).
- Den studerende har viden om security by design principper (Domain primitives).
- Den studerende kan bruge et API og/eller et standard biblotek.
- Den studerende kan opdage og forhindre sårbarheder i koden.
- Den studerende kan definere lovlige og ikke-lovlige input data, bla. test. (I koden)
- Den studerende kan håndterer risikovurdering af programkode for sårbarheder.

## Skema
### Torsdag
|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen samt opsamlig fra forrige lektion k1 |
| 08:45  | Videndelings øvelse i Teams K2  4          |
| 09:15 |      Opsamling på vidensdelings øvelser   k1 |
| 09:30 | Programmerings øvelser og taint analyse k1/k4 |
| 1130  | Frokost pause |
| 1215  | Arbejde med Python/Flask Applikation samt gruppe samtaler |
| 1530  | Aftrædelse |

## Øvelser

Se denne uges øvelser: [https://ucl-pba-its.gitlab.io/exercises/software/software_opgave_Uge40/](https://ucl-pba-its.gitlab.io/exercises/software/software_opgave_Uge40/)

## Kommentarer

- ..
